//PCW9121D PROC DBID=045,
//       NATVERS='NATB030',
//       REGN1='8M',
//       NAT='NAT003P',                                                 
//       DMPS='U',
//       JCLO='*',
//       LIB1='PROD',
//       HLQ1='PCWF.ANN',
//       HLQ2='PDA.ONL.ANN',
//       JOBNM='PCW9121D',
//       NATMEM='PANNSEC',                                              
//       REPT='8',
//       SPC1='10',
//       SPC2='5'
//*-------------------------------------------------------------------*
//* Berwyn Automation - Log CWF Requests
//* ----------------------------------------------------------------- *
//* Copy Input Berwyn Data to Work File
//*
//* 02/05/2013 - Work file is changed as a GDG per new rule in NY to
//*              have the backup of the Berwyn file for at least an year
//* ----------------------------------------------------------------- *
//COPY010 EXEC PGM=IDCAMS,COND=EVEN
//SYSPRINT  DD SYSOUT=*
//INDD      DD DSN=&HLQ1..&JOBNM..BERWYN,DISP=SHR
//OUDD      DD DSN=&HLQ1..&JOBNM..BERWYN.RCVD(+1),
//             DISP=(NEW,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             RECFM=LS,LRECL=141
//SYSIN     DD DSN=&LIB1..PARMLIB(CW9121D1),DISP=SHR
//* ----------------------------------------------------------------- *
//* Create MIT Work Requests and Electronic Folder Documents          *
//* ----------------------------------------------------------------- *
//UPDT020  EXEC PGM=&NATVERS,REGION=&REGN1,COND=(0,NE),
//         PARM='SYS=&NAT,OPRB=(.ALL)'
//SYSPRINT DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDPRINT  DD  SYSOUT=&JCLO
//DARFACT  DD  DSN=&HLQ2..DAR.FACTORS.FILE.V431,DISP=SHR
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=(&REPT,CW9121D1)
//CMWKF01  DD  DSN=&HLQ1..&JOBNM..BERWYN.RCVD(+1),DISP=SHR
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(CW9121DA),DISP=SHR
//* ----------------------------------------------------------------- *
//* De-Allocate Berwyn Input Work File
//* ----------------------------------------------------------------- *
//DELT030  EXEC PGM=IEFBR14,COND=(0,NE)
//SYSPRINT DD  SYSOUT=&JCLO
//TMPFL01  DD  DSN=&HLQ1..&JOBNM..BERWYN,
//             UNIT=SYSDA,
//             DISP=(MOD,DELETE,DELETE),
//             SPACE=(CYL,(0,0))
//SYSIN    DD  DUMMY
