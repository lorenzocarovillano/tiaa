//PCI2120D PROC DBID=003,
//         HLQ1='PPM.ANN',
//         HLQ6='PNA.ANN',
//         HLQ8='PPDD.PCI2120D',
//         JCLO='*',
//         LIB1='PROD',
//         REGN1='9M',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         GDG1='+1',                                                   
//         SPC1='5,5'
//*
//**********************************************************************
//* MAINTENANCE LOG                                                    *
//*  DATE                                                              *
//* MM/YYYY ANALYST        DESCRIPTION                           TAG   *
//* ------- -------------  ------------------------------------  ----- *
//* 08/2012 B HOLLOWAY     DOCUMERG TO DIALOGUE CONVERSION FOR         *
//*                        IMMEDIATE ANNUITY PRODUCTS (IA)             *
//*                        REMOVE THE SORT INCLUDE USED TO STAGE       *
//*                        IMPLEMENTATIONS BY PRODUCT.  ALL            *
//*                        PRODUCTS ARE NOW GOING TO DIALOGUE SO       *
//*                        SELECTING PRODUCTS IS NOT NECESSARY.        *
//*                                                                    *
//**********************************************************************
//*
//**********************************************************************
//* ALOC010: PRE-ALLOCATE OUTPUT FILES                                 *
//**********************************************************************
//ALOC010  EXEC PGM=IEFBR14,REGION=&REGN1
//ALOCMDO  DD DSN=&HLQ1..CIS.REPRINT.MDO.DIALOG.DATA,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             DCB=(RECFM=VB,BLKSIZE=23476),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//ALOCIA   DD DSN=&HLQ1..CIS.REPRINT.IA.DIALOG.DATA,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             DCB=(RECFM=VB,BLKSIZE=23476),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//*
//**********************************************************************
//* SORT025: CHANGE LOW VALUES TO SPACES                               *
//**********************************************************************
//SORT025  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSPRINT DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SORTIN   DD DSN=&HLQ1..DOCUMERG.REPRINT.EXTRACT,DISP=SHR
//SORTOUT  DD DSN=&HLQ1..DOCUMERG.REPRINT.DIAG,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=LS,LRECL=17659,BLKSIZE=0),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SYSIN    DD  DSN=&LIB1..PARMLIB(CI2120D7),DISP=SHR
//*
//**********************************************************************
//* VERY030: CHECK FOR EMPTY INPUT                                     *
//**********************************************************************
//VERY030  EXEC PGM=IDCAMS
//SYSPRINT DD DUMMY
//DDIN     DD DSN=&HLQ1..DOCUMERG.REPRINT.DIAG,DISP=SHR
//SYSIN    DD DSN=&LIB1..PARMLIB(CI2120D2),DISP=SHR
//*
//**********************************************************************
//* FRMT040: ADD THE JOBNAME TO THE EXTRACT FILE - PROGRAM CIS1006.    *
//**********************************************************************
//FRMT040 EXEC PGM=CIS1006,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=*
//SYSABOUT DD SYSOUT=*,
//            DCB=(RECFM=LSM,LRECL=133,BLKSIZE=1330)
//SYSUDUMP DD SYSOUT=U,
//            DCB=(RECFM=LSM,LRECL=133,BLKSIZE=1330)
//SYSPRINT DD SYSOUT=*,
//            DCB=(RECFM=LSM,LRECL=133,BLKSIZE=1330)
//NROLDATA DD DSN=&HLQ1..DOCUMERG.REPRINT.DIAG,DISP=SHR
//CPDATA   DD DSN=&HLQ6..CIS.DIAG.RULES,DISP=SHR
//TEMPNROL DD DSN=&HLQ1..CIS.WORK.PCI2120D,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=LS,LRECL=17699,BLKSIZE=0),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//*
//**********************************************************************
//* SORT050: SORT BY JOBNAME, PRODUCT, CONTRACT TYPE, CONTRACT NUMBER. *
//**********************************************************************
//SORT050  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSPRINT DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SORTIN   DD DSN=&HLQ1..CIS.WORK.PCI2120D,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..CIS.WORK.SORT.PCI2120D,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=LS,LRECL=17699,BLKSIZE=0),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SYSIN    DD  DSN=&LIB1..PARMLIB(CI2120D3),DISP=SHR
//*
//**********************************************************************
//* FRMT060: CREATE DIALOG FEED FILES. PROGRAM CISB610.                *
//**********************************************************************
//FRMT060  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01  DD DSN=&HLQ8..CIS.WORK.SORT.PCI2120D,DISP=SHR
//CMWKF02  DD DSN=&HLQ1..CIS.REPRINT.IA.DIALOG.DATA,DISP=SHR
//CMWKF03  DD DSN=&HLQ1..CIS.REPRINT.MDO.DIALOG.DATA,DISP=SHR
//SYSPRINT DD SYSOUT=*
//DDPRINT  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=*
//CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(CI2120D4),DISP=SHR
//*
//*********************************************************************
//* COPY070: COPY THE DIALOG IA FEED FILE TO A BACKUP GDG.            *
//*********************************************************************
//COPY070  EXEC PGM=IEBGENER,COND=(0,NE)
//SYSPRINT DD  SYSOUT=&JCLO
//SYSUT1   DD  DSN=&HLQ1..CIS.REPRINT.IA.DIALOG.DATA,DISP=SHR
//SYSUT2   DD  DSN=&HLQ1..CIS.REPRINT.IA.DIALOG.BU(&GDG1),
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             DCB=(RECFM=VB),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SYSIN    DD  DUMMY
//*
//*********************************************************************
//* COPY075: THE DIALOG MDO FEED FILE TO A BACKUP GDG.                *
//*********************************************************************
//COPY075  EXEC PGM=IEBGENER,COND=(0,NE)
//SYSPRINT DD  SYSOUT=&JCLO
//SYSUT1   DD  DSN=&HLQ1..CIS.REPRINT.MDO.DIALOG.DATA,DISP=SHR
//SYSUT2   DD  DSN=&HLQ1..CIS.REPRINT.MDO.DIALOG.BU(&GDG1),
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             DCB=(RECFM=VB),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SYSIN    DD  DUMMY
//*
//*********************************************************************
//* SORT080: SORT CIS1006 OUTPUT FILE FOR REPORT CISB800.             *
//*********************************************************************
//SORT080  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSPRINT DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SORTIN   DD DSN=&HLQ1..CIS.WORK.PCI2120D,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..CIS.WORK.PCI2120D.REPT,
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            DCB=(RECFM=LS,LRECL=7040,BLKSIZE=0),
//            UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SYSIN    DD  DSN=&LIB1..PARMLIB(CI2120D5),DISP=SHR
//*
//********************************************************************
//* REPT090: DETAIL AND SUMMARY REPORT                               *
//********************************************************************
//REPT090  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT'
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01  DD DSN=&HLQ8..CIS.WORK.PCI2120D.REPT,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//DDPRINT  DD  SYSOUT=*
//SYSOUT   DD  SYSOUT=*
//SYSUDUMP DD  SYSOUT=U
//CMPRINT  DD  SYSOUT=*
//CMPRT01  DD SYSOUT=(,CI2120D1),OUTPUT=(*.OUT1,*.OUT2),
//            DCB=(RECFM=LSM,LRECL=133,BLKSIZE=1330)
//CMPRT02  DD SYSOUT=(,CI2120D2),OUTPUT=(*.OUT1,*.OUT2),
//            DCB=(RECFM=LSM,LRECL=133,BLKSIZE=1330)
//CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(CI2120D6),DISP=SHR
