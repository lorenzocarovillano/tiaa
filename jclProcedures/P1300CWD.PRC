//P1300CWD PROC DBID='045',
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD',
//         NAT='NAT045P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8'
//*        SPC1='25,20',
//*        SPC2='25,20'
//**********************************************************************
//* STEP ID      : REPT010                                             *
//* DESCRIPTION  : PRODUCE OPEN AND BATCH FORMATTING BATCHES           *
//*              : (TRACKING SLIP) REPORT                              *
//* PROGRAM NAME : CWFB5000                                            *
//* ADABAS FILES : DB045 - FILE 181 CWF-MASTER-INDEX-VIEW              *
//* REMARKS      :                                                     *
//* -------------------------------------------------------------------*
//* CHANGES      :                                                     *
//*                                                                    *
//*    7/15/94 - CHANGED STEPLIB AS PER DONNA BUDALL                   *
//*    9/20/94 - CHANGED NP2045P TO NT2045P.       A.A.                *
//*    5/25/95 - ADDED CMPRT02                     J.S.PATINGO         *
//*   11/07/95 - ADDED SECOND STEP                 L.EPSTEIN           *
//*   10/30/02 - PERMANENT WORK FILE  CREATED      L.EPSTEIN           *
//*   10/30/02 - INITIALIZE STEP HAS BEEN ADDED    L.EPSTEIN           *
//**********************************************************************
//**********************************************************************
//*    INITIALIZE WORK FILES                                           *
//**********************************************************************
//INIT010  EXEC PGM=IDCAMS
//DD1      DD  DUMMY
//DD2      DD DSN=PCWF.COR.P1300CWD.SLIP.FILE,
//         DISP=OLD
//*
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  DSN=PROD.PARMLIB(P1300CW3),DISP=SHR
//*
//**********************************************************************
//REPT010  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//SYSPRINT DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMWKF02  DD  DSN=PCWF.COR.P1300CWD.SLIP.FILE,DISP=SHR,
//             UNIT=SYSDA,
//             SPACE=(CYL,(25,20),RLSE),
//             RECFM=LS,LRECL=9,BLKSIZE=27990
//**           DISP=(NEW,PASS),
//**           RECFM=LS,LRECL=9,BLKSIZE=1800
//CMPRT01  DD  SYSOUT=(&REPT,CW1300D1)
//CMPRT02  DD  SYSOUT=(&REPT,CW1300D2)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1300CW1),DISP=SHR
//**********************************************************************
//* THIS STEP NO LONGER NEEDED - J BREMER 2016 COR/NAAD RETIREMENT  ***
//**********************************************************************
//*REPT011  EXEC PGM=NATB030,REGION=&REGN1,
//*         PARM='SYS=&NAT'
//*DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//*SYSOUT   DD  SYSOUT=&JCLO
//*SYSPRINT DD  SYSOUT=&JCLO
//*DDPRINT  DD  SYSOUT=&JCLO
//*CMPRINT  DD  SYSOUT=&JCLO
//*SYSUDUMP DD  SYSOUT=&DMPS
//*CMWKF01  DD  SYSOUT=&JCLO
//*CMWKF02  DD  DSN=PCWF.COR.P1300CWD.SLIP.FILE,DISP=SHR
//*CMPRT01  DD  SYSOUT=(&REPT,CW1300D3),OUTPUT=(*.OUT1,*.OUT2)
//*CMPRT02  DD  SYSOUT=(&REPT,CW1300D4),OUTPUT=(*.OUT1,*.OUT2)
//*CMPRT03  DD  SYSOUT=(&REPT,CW1300D5)
//*CMPRT04  DD  SYSOUT=(&REPT,CW1300D6)
//*CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//*         DD  DSN=&LIB1..PARMLIB(P1300CW2),DISP=SHR
