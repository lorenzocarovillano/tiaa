//P5130IAM PROC DBID='003',
//         DMPS='U',
//         GDG0='+0',
//         GDG1='+1',
//         HLQ0='PIA.ANN',
//         HLQ1='PIA.ONL.ANN',
//***      HLQ2='PINT.ONL.ANN',
//         HLQ3='PIA.ANN.IA',
//         HLQ8='PPDD',
//         JCLO='*',
//         JOBNM='P5130IAM',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REGN2='8M',
//         REPT='8',
//         SPC1='20',
//         SPC2='5',
//         SPC3='5',
//         SPC4='5'
//*
//* ----------------------------------------------------------------- * 
//* PROCEDURE:  P5130IAM                 DATE:     3/25/96            * 
//* ----------------------------------------------------------------- * 
//* PROGRAMS EXECUTED: IAAP340 (NATURAL)  SORT                        * 
//* ----------------------------------------------------------------- * 
//* CREATE  AUTO GENERATED TRANSACTIONS 006(TERMINATIONS),700(IVC)    *
//*         & 900(DEDUCTIONS) FROM IAIQ FLAT FILE TO BE APPLIED TO    *
//*         IAIQ DATABASE FILES BY ANOTHER JOB                        *
//*                                                                   *
//* MODIFICATION HISTORY
//* 03/98      ADDED PIA.ONL.ANN.DAILYAUV.V471
//* 08/12/98   ADDED  OPRB PARAMETER
//* 04/11/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//* 06/2017  - REMOVED LRECL                                            
//**********************************************************************
//*
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM=('SYS=&NAT',
//       'OPRB=(.DBID=253,FNR=043,I,A)'),COND=(0,NE)
//SYSPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01   DD SYSOUT=(&REPT,IA5130M1)
//SYSUDUMP  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMWKF01  DD  DSN=&HLQ0..IAIQ.SUMMARY.MASTER.FILE(&GDG0),
//             DISP=SHR                                                 
//*                                                                     
//CMWKF02   DD DSN=&HLQ8..&JOBNM..EXT1,                        OUTPT    
//             DISP=(NEW,CATLG,DELETE),                                 
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),               
//*            DCB=(MODLDSCB,RECFM=LS,LRECL=214)                        
//             DCB=(MODLDSCB,RECFM=LS)                                  
//DIAUVS    DD DSN=&HLQ1..DAILYAUV.V471,DISP=SHR
//*** IAA470    DD DSN=&HLQ2..CREFFANN.UNITVALS.V164,DISP=SHR
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P5130IA1),DISP=SHR          IAAP340
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*------------------------------------------------------------------*
//*  SORT  EXTRACT IN  TRAN-CODE, INTENT-CODE, CONTRACT-NBR
//*------------------------------------------------------------------ *
//SORT020  EXEC PGM=SORT,REGION=&REGN2,COND=(0,NE)                      
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR                             
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC3,&SPC4))                     
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC3,&SPC4))                     
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC3,&SPC4))                     
//SORTIN    DD DSN=&HLQ8..&JOBNM..EXT1,                      INPUT      
//             DISP=(SHR,DELETE,KEEP),                                  
//             UNIT=SYSDA                                               
//SORTOUT   DD DSN=&HLQ3..AUTOGEN.TRANS(&GDG1),             OUTPT       
//             DISP=(NEW,CATLG,DELETE),                                 
//             UNIT=SYSDA,SPACE=(TRK,(&SPC3,&SPC4),RLSE),               
//*            DCB=(MODLDSCB,RECFM=LS,LRECL=214)                        
//             DCB=(MODLDSCB,RECFM=LS)                                  
//SYSIN    DD  DSN=&LIB1..PARMLIB(P5130IA2),DISP=SHR                    
