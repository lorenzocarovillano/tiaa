//PCI9900D PROC DMPS='U',
//         JCLO='*',
//         REGN1='0M',
//         HLQ1='PPM.ANN',
//         DATACLS='DCPSEXTC',
//         GEN1=(+1),
//         LIB1='PROD',
//         LIB2='PROD',
//         SPC1='5,2',
//         NAT='NAT045P'
//*****************************************************************
//* THIS JOB WILL RUN BEFORE P1020CID AND P3000CID AND WILL SEND  *
//* DATA TO PCI VIA MQ.  THIS IS A TEMPORARY PROCESS AND WILL RUN *
//* UNTIL THE IA PROCESS IS OPERATIONAL.                          *
//*****************************************************************
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,
//          PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP045),DISP=SHR
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ1..DATA.SENT.TO.CPI&GEN1,
//            DISP=(NEW,CATLG,DELETE),DATACLAS=&DATACLS,
//            UNIT=SYSDA,
//            DCB=RECFM=LS,
//            SPACE=(CYL,(&SPC1),RLSE)
//CMWKF02  DD DUMMY
//SYSUDUMP DD SYSOUT=&DMPS
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(PANNSEC),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(CI9900D1),DISP=SHR
