//PIA2250D PROC DBID='003',
//         DMPS='U',
//         GDG0='+0',
//         GDG1='+1',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         HLQ0='PIA.ANN.IA',
//         HLQ1='PPDD.ANN',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='0M',
//         REPT='8',
//         SPC1='2',
//         SPC2='1',
//         SPC3='5'
//*
//********************************************************************
//* PROCEDURE:  PIA2250D             DATE:  02/28/2017
//*
//* PROCESSES A FEED FROM IADB FOR ITD ACCELERATED ANNUITIZATION. THIS
//* WILL RUN DAILY ON BUSINESS DAYS.
//*                                                                     
//* INPUT  FILE: PIA.ANN.IA.ITD.INFILE(0) - FROM AUTOSYS
//* OUTPUT FILE: PIA.ANN.IA.STATUS.UPDT(+1) - TO CTH
//* OUTPUT FILE: PIA.ANN.IA.STTLMNT(+1) - TO ESP/FSDF - DATA
//* OUTPUT FILE: PPDD.ANN.IAITD.CTL - TO ESP/FSDF - TRIGGER
//**********************************************************************
//*                                                                     
//* MODIFICATION HISTORY
//* 02/28/17 - JUN TINIO - ORIGINAL CODE
//*                                                                     
//********************************************************************
//* EXECUTES PROGRAM IAAP760
//*******************************************************
//UPDT010  EXEC PGM=NATB030,COND=(0,NE),REGION=&REGN1,
//         PARM='SYS=&NAT'
//SYSPRINT DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDCARD   DD DSN=&LIB2..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01  DD SYSOUT=(&REPT,IA2250D1)
//CMPRT02  DD SYSOUT=(&REPT,IA2250D2)
//CMWKF01  DD DSN=&HLQ0..ITD.INFILE(&GDG0),DISP=SHR
//CMWKF02  DD DSN=&HLQ0..STATUS.UPDT(&GDG1),
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            DATACLAS=DCPSEXTC,
//            DCB=RECFM=LS,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//*
//CMWKF03  DD DSN=&HLQ0..STTLMNT(&GDG1),
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            DATACLAS=DCPSEXTC,
//            DCB=RECFM=LS,
//            SPACE=(CYL,(&SPC3,&SPC2),RLSE)
//*
//CMWKF04  DD DSN=&HLQ1..IAITD.CTL,
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            DATACLAS=DCPSEXTC,
//            DCB=RECFM=LS,
//            SPACE=(CYL,(&SPC2,0),RLSE)
//*
//SYSUDUMP DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(IA2250D1),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
