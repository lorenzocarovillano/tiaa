//P5125NIR PROC                                                         
//*------------------------------------------------------------------*  
//**                                                                    
//* EXTRACT CONTRACT NUMBERS FROM THE S250 FILE AND WRITE TO WORK DSN.  
//*        --------------------------------------------------           
//*            THIS JCL SHOULD BE STAGED IN CA-SCHEDULER.               
//**                                                                    
//*------------------------------------------------------------------*  
//SORT0010 EXEC PGM=SORT,                                               
//             REGION=5M                                                
//SORTWK01 DD  SPACE=(CYL,(55,35)),                                     
//             UNIT=SYSDA                                               
//SYSOUT   DD  SYSOUT=*                                                 
//*                                                   S250              
//SORTIN   DD  DSN=PDNT.P1110NID.S250.BU(0),          DENVER COPY OF    
//             DISP=SHR                                                 
//SORTOUT  DD  DSN=PPDD.DMG.ACIS.DN.EXTRACT,                            
//             DISP=(NEW,CATLG),                                        
//             DCB=(BLKSIZE=30000,                                      
//             DSORG=PS,                                                
//             LRECL=2500,                                              
//             RECFM=LS),                                               
//             SPACE=(CYL,(3,2)),                                       
//             UNIT=SYSDA                                               
//SYSIN    DD  DSN=PROD.DN.CNTL.PARMLIB(P5125NI1),                      
//             DISP=SHR                                                 
//STEP020  EXEC PGM=IEBGENER                                            
//SYSIN    DD  DUMMY                                                    
//SYSPRINT DD  DUMMY                                                    
//SYSUT1   DD  DSN=PPDD.DMG.ACIS.DN.EXTRACT,DISP=SHR                    
//SYSUT2   DD  SYSOUT=*                                                 
//         PEND                                                         
