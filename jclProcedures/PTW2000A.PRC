//PTW2000A PROC DBID=003,
//         PRGMN=030,
//         DMPS='U',
//         GDG1='+1',
//         HLQ7='PPDT.TAX',
//         HLQ0='PNPD.TAX',
//         HLQ8='PPDD.TAX',
//         JCLO='*',
//         JOBNM1='PTW2000A',
//         JOBNM2='P1500TWA',
//         LIB1='PROD',     PARMLIB
//         LIB2='PROD',     NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='8M',
//         REPT='8',
//         REPID1='TW2000A1',
//         REPID2='TW2000A2',
//         REPID3='TW2000A3',
//         REPID4='TW2000A4',
//         REPID5='TW2000A5',
//         REPID6='TW2000A6',
//*        SPC1='150,50',
//         SPC8='500,250',
//         UNIT1='CARTV'
//**********************************************************************
//* 07/29/2012 - RRD FOR SUNY
//* *******************************************************************
//**********************************************************************
//* PROCEDURE: PTW2000A TAX & WITHHOLDING REPORTING                    *
//*            TAX REPORTING -                                         *
//*            CREATES THE ANNUAL TAX TAPE FROM THE FORM FILE          *
//*            FOR MASS MAILING OF RRD FOR SUNY                        *
//*                                                                    *
//*******************************************************************
//*EXTR010 - EXTRACTS ACTIVE TAX FORM DATA FROM THE FORM FILE
//********************************************************************
//EXTR010 EXEC PGM=NATB&PRGMN,TIME=1400,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRT01   DD SYSOUT=(&REPT,&REPID1),
//             RECFM=LS,LRECL=133,BLKSIZE=11571
//CMPRT04   DD SYSOUT=(&REPT,&REPID2),
//             RECFM=LS,LRECL=133,BLKSIZE=11571
//CMWKF01   DD DSN=&HLQ7..&JOBNM1..EXTR010.OUT(&GDG1),
//             DISP=(,CATLG,DELETE),
//             UNIT=&UNIT1,
//             DCB=MODLDSCB,RECFM=LS,LRECL=1000
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(TW2000A1),DISP=SHR
//* TWRP2100
//*********************************************************************
//*FRMT020 - CREATES THE MASS MAILING OUTPUT IMAGE FILE
//*****************************************************************
//FRMT020 EXEC PGM=NATB&PRGMN,TIME=1400,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=(&REPT,&REPID3),
//             RECFM=LS,LRECL=133,BLKSIZE=11571
//CMPRT02   DD SYSOUT=(&REPT,&REPID4),
//             RECFM=LS,LRECL=133,BLKSIZE=11571
//CMPRT03   DD SYSOUT=(&REPT,&REPID5),
//             RECFM=LS,LRECL=133,BLKSIZE=11571
//SYSUDUMP  DD SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01   DD DSN=&HLQ7..&JOBNM1..EXTR010.OUT(&GDG1),
//             DISP=OLD,
//             UNIT=&UNIT1
//CMWKF02   DD DSN=&HLQ7..&JOBNM1..FRMT020.OUT(&GDG1),
//             DCB=MODLDSCB,RECFM=LS,LRECL=507,
//             DISP=(,CATLG,DELETE),UNIT=&UNIT1
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(TW2000A2),DISP=SHR
//* TWRP2110
//*********************************************************************
//*MERG030- PRINTS THE FORMATTED DUMP OF THE MASS MAILING OUTPUT FILE.
//*          SUMS UP THE FIELDS OF DETAIL RECORDS AND COMPARES THEM
//*          WITH THE ONES OF THE TOTAL RECORDS.
//*****************************************************************
//MERG030 EXEC PGM=NATB&PRGMN,TIME=1400,REGION=&REGN1,COND=(0,NE),
//         PARM='SYS=&NAT'
//*       PARM='IM=D,SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//CMWKF01   DD DSN=&HLQ0..&JOBNM2..F1099R.SERV.SRTED,DISP=SHR FROM 1099
//CMWKF02   DD DSN=&HLQ7..&JOBNM1..FRMT020.OUT(&GDG1),DISP=OLD,
//             UNIT=&UNIT1
//CMWKF03  DD DSN=&HLQ8..&JOBNM1..MERGE.RECS,
//             DISP=(NEW,CATLG,DELETE),
//             LRECL=80,RECFM=LS,
//             SPACE=(CYL,(&SPC8),RLSE),UNIT=SYSDA
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(TW2000A3),DISP=SHR
//* TWRP2140
//*-------------------------------------------------------------------
//*SORT040 - SORT 1099 AND SUNY RRD BY TIN
//*-------------------------------------------------------------------
//SORT040  EXEC PGM=SORT,REGION=0M,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC8),RLSE)
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(&SPC8),RLSE)
//SORTWK03 DD UNIT=SYSDA,SPACE=(CYL,(&SPC8),RLSE)
//SORTWK04 DD UNIT=SYSDA,SPACE=(CYL,(&SPC8),RLSE)
//SYSPRINT DD SYSOUT=&JCLO
//SORTIN   DD DSN=&HLQ8..&JOBNM1..MERGE.RECS,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..&JOBNM1..MERGED.RECS.SORTED,
//             DISP=(NEW,CATLG,DELETE),
//             LRECL=80,RECFM=LS,
//             SPACE=(CYL,(&SPC8),RLSE),UNIT=SYSDA
//*
//SYSIN    DD DSN=&LIB1..PARMLIB(TW2000A5),DISP=SHR
//* SORT FIELDS=(1,13,CH,A)   <<  TIN+4 POS
//*--------------------------------------------------------------------
//* PRNT050 - STEP PRODUCES A REPORT OF THE NUMBER OF CONTRIBUTORS WITH
//*           ONE, TWO, THREE ... PAGES UP.
//*           NATURAL PROGRAM "TWRP2150".
//*--------------------------------------------------------------------
//PRNT050  EXEC PGM=NATB030,REGION=0M,
//         PARM='SYS=&NAT,UDB=&DBID,OBJIN=N,IM=D'
//SYSPRINT  DD SYSOUT=&JCLO
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//DDPRINT   DD SYSOUT=&JCLO
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=(&REPT,&REPID6)
//*
//CMWKF01  DD DSN=&HLQ8..&JOBNM1..MERGED.RECS.SORTED,DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(TW2000A4),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*RUN TWRP2150
//********************************************************************
//*BKUP060 -  SENT TO RRD
//********************************************************************
//*BKUP060 EXEC PGM=IEBGENER,COND=(0,NE)
//*SYSPRINT  DD SYSOUT=&JCLO
//*SYSUT1    DD DSN=&HLQ7..&JOBNM1..FRMT020.OUT(&GDG1),DISP=SHR
//*SYSUT2    DD DSN=&HLQ7..&JOBNM1..BK060.RRD(&GDG1),
//*             DISP=(,CATLG,DELETE),
//*             UNIT=&UNIT1,
//*             DCB=MODLDSCB,RECFM=LS,LRECL=507
//*SYSIN     DD DUMMY
//*
