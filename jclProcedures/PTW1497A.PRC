//**********************************************************************
//*                          T A X W A R S                             *
//*                  RECONCILIATION REPORTS FOR 5498                   *
//*                          (PRODUCTION)                              *
//**********************************************************************
//PTW1497A PROC DBID='003',                                             
//         DMPS='*',                                                    
//         HLQ0='PNPD.COR.TAX',                                         
//         JCLO='*',                                                    
//         JOBNM='P1497TWA',                                            
//         JOBNM1='P1496TWA',                                           
//         JOBNM2='P1495TWA',                                           
//         LIB1='PROD',                                                 
//         LIB2='PROD',                                                 
//         NAT='NAT003P',                                               
//         NATMEM='PANNSEC',                                            
//         REGN1='8M',                                                  
//         REPT='8',                                                    
//         REPT1='TW1497A',                                             
//         SPC1='100',                                                  
//         SPC2='50',                                                   
//*        SPC3='40,5',                                                 
//*        SPC4='1,1',                                                  
//         DATACLS='DCPSEXTC',                                          
//         UNIT='SYSDA',                                                
//         UNIT1='CARTV'                                                
//*        UNIT2='CARTN'                                                
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//* PROCEDURE: P1497TWA                                               * 
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//*-------------------------------------------------------------------- 
//* DELETE - DELETE THE MERGED AND SORTED FILES                         
//*-------------------------------------------------------------------- 
//DELT005 EXEC PGM=IEFBR14                                              
//SYSOUT   DD  SYSOUT=&JCLO                                             
//DD1      DD  DSN=&HLQ0..&JOBNM..RECON.F5498.ALL,                      
//             UNIT=SYSDA,SPACE=(TRK,0),                                
//             DISP=(MOD,DELETE,DELETE)                                 
//DD2      DD  DSN=&HLQ0..&JOBNM..RECON.F5498.ALL.SRTED,                
//             UNIT=SYSDA,SPACE=(TRK,0),                                
//             DISP=(MOD,DELETE,DELETE)                                 
//DD3      DD  DSN=&HLQ0..&JOBNM..RECON.F5498.P1,                       
//             UNIT=SYSDA,SPACE=(TRK,0),                                
//             DISP=(MOD,DELETE,DELETE)                                 
//DD4      DD  DSN=&HLQ0..&JOBNM..RECON.F5498.P2,                       
//             UNIT=SYSDA,SPACE=(TRK,0),                                
//             DISP=(MOD,DELETE,DELETE)                                 
//*                                                                     
//*-------------------------------------------------------------------- 
//* MERG010 - MERGE ALL 5498 E-DELIVERY & PAPER RECON FILES             
//*-------------------------------------------------------------------- 
//MERG010  EXEC PGM=SORT                                                
//SYSPRINT DD  SYSOUT=&JCLO                                             
//SORTIN   DD  DSN=&HLQ0..&JOBNM1..RECON.F5498.E.F1,DISP=SHR            
//         DD  DSN=&HLQ0..&JOBNM1..RECON.F5498.E.F2,DISP=SHR            
//         DD  DSN=&HLQ0..&JOBNM1..RECON.F5498.E.F3,DISP=SHR            
//         DD  DSN=&HLQ0..&JOBNM1..RECON.F5498.E.F4,DISP=SHR            
//         DD  DSN=&HLQ0..&JOBNM1..RECON.F5498.E.F5,DISP=SHR            
//         DD  DSN=&HLQ0..&JOBNM1..RECON.F5498.E.F6,DISP=SHR            
//         DD  DSN=&HLQ0..&JOBNM1..RECON.F5498.E.F7,DISP=SHR            
//         DD  DSN=&HLQ0..&JOBNM1..RECON.F5498.E.F8,DISP=SHR            
//         DD  DSN=&HLQ0..&JOBNM1..RECON.F5498.E.F9,DISP=SHR            
//         DD  DSN=&HLQ0..&JOBNM1..RECON.F5498.E.F10,DISP=SHR           
//         DD  DSN=&HLQ0..&JOBNM1..RECON.F5498.E.F11,DISP=SHR           
//         DD  DSN=&HLQ0..&JOBNM1..RECON.F5498.E.F12,DISP=SHR           
//         DD  DSN=&HLQ0..&JOBNM1..RECON.F5498.E.F13,DISP=SHR           
//         DD  DSN=&HLQ0..&JOBNM1..RECON.F5498.E.F14,DISP=SHR           
//         DD  DSN=&HLQ0..&JOBNM1..RECON.F5498.E.F15,DISP=SHR           
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F1,DISP=SHR              
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F2,DISP=SHR              
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F3,DISP=SHR              
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F4,DISP=SHR              
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F5,DISP=SHR              
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F6,DISP=SHR              
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F7,DISP=SHR              
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F8,DISP=SHR              
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F9,DISP=SHR              
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F10,DISP=SHR             
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F11,DISP=SHR             
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F12,DISP=SHR             
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F13,DISP=SHR             
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F14,DISP=SHR             
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F15,DISP=SHR             
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F16,DISP=SHR             
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F17,DISP=SHR             
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F18,DISP=SHR             
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F19,DISP=SHR             
//         DD  DSN=&HLQ0..&JOBNM2..RECON.F5498.F20,DISP=SHR             
//SORTOUT  DD  DSN=&HLQ0..&JOBNM..RECON.F5498.ALL,                      
//             DISP=(NEW,CATLG,DELETE),                                 
//             DCB=(RECFM=LS,LRECL=1005),                               
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT,               
//             DATACLAS=&DATACLS                                        
//SYSIN    DD  DSN=&LIB1..PARMLIB(TW1497A1),DISP=SHR                    
//SYSOUT   DD  SYSOUT=&JCLO                                             
//*                                                                     
//*-------------------------------------------------------------------- 
//* REPORT - 5498 RECON REPORT USING PROGRAM TWRP4711                   
//*-------------------------------------------------------------------- 
//REPT020  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//    PARM='SYS=&NAT'                                                   
//SYSPRINT DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSOUT   DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMPRT01  DD  SYSOUT=(&REPT,&REPT1.A),                                 
//             RECFM=LS,LRECL=133,BLKSIZE=11571                        
//CMPRT02  DD  SYSOUT=(&REPT,&REPT1.B),                                 
//             RECFM=LS,LRECL=133,BLKSIZE=11571                        
//CMPRT03  DD  SYSOUT=(&REPT,&REPT1.C),                                 
//             RECFM=LS,LRECL=133,BLKSIZE=11571                        
//CMWKF01  DD DSN=&HLQ0..&JOBNM..RECON.F5498.ALL,DISP=SHR               
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(TW1497A2),DISP=SHR  /* TWRP4711        
//         DD DSN=&LIB1..PARMLIB(TW1497A9),DISP=SHR  /* PARM Y1         
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                      
//*                                                                     
//********************************************************************* 
//* SORT - SORTS 5498 FILE TO SELECT GOOD PAPER FORMS                   
//********************************************************************* 
//SORT030 EXEC PGM=SORT,COND=(0,NE)                                     
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)                      
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)                      
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)                      
//SORTIN    DD DSN=&HLQ0..&JOBNM..RECON.F5498.ALL,DISP=SHR,             
//             UNIT=&UNIT1                                              
//SORTOUT   DD DSN=&HLQ0..&JOBNM..RECON.F5498.ALL.SRTED,                
//             DISP=(NEW,CATLG,DELETE),                                 
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT,               
//             DCB=MODLDSCB,RECFM=LS,LRECL=1005                         
//SYSIN     DD DSN=&LIB1..PARMLIB(TW1570A8),DISP=SHR                    
//**********************************************************************
//* REPORT - 5498 ENVELOPE REPORT USING PGM TWRP2004                    
//********************************************************************* 
//REPT040  EXEC PGM=NATB030,REGION=8M,COND=(0,NE),                      
//         PARM='SYS=&NAT'                                              
//SYSPRINT  DD SYSOUT=&JCLO                                             
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01   DD SYSOUT=(&REPT,&REPT1.D),                                 
//             RECFM=LS,LRECL=133,BLKSIZE=11571                        
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//CMWKF01   DD DSN=&HLQ0..&JOBNM..RECON.F5498.ALL.SRTED,DISP=SHR        
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(TW1497A3),DISP=SHR   /* TWRP2004      
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//********************************************************************* 
//* REPORT - 5498 BYPASSED FORM REPORT USING PGM TWRP2005               
//********************************************************************* 
//REPT050  EXEC PGM=NATB030,REGION=8M,COND=(0,NE),                      
//         PARM='SYS=&NAT'                                              
//SYSPRINT  DD SYSOUT=&JCLO                                             
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01   DD SYSOUT=(&REPT,&REPT1.E),                  /* E DELIVERY  
//             RECFM=LS,LRECL=133,BLKSIZE=11571                        
//CMPRT02   DD SYSOUT=(&REPT,&REPT1.F),                  /* PAPER       
//             RECFM=LS,LRECL=133,BLKSIZE=11571                        
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//CMWKF01   DD DSN=&HLQ0..&JOBNM..RECON.F5498.ALL,DISP=SHR              
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(TW1497A4),DISP=SHR     /* TWRP2005,04 
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//**********************************************************************
//* REPORT - 5498 COUPON COUNTS REPORT USING PGM TWRP2006               
//********************************************************************* 
//REPT060  EXEC PGM=NATB030,REGION=8M,COND=(0,NE),                      
//         PARM='SYS=&NAT'                                              
//SYSPRINT  DD SYSOUT=&JCLO                                             
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01   DD SYSOUT=(&REPT,&REPT1.G),                                 
//             RECFM=LS,LRECL=133,BLKSIZE=11571                        
//CMWKF01   DD DSN=&HLQ0..&JOBNM..RECON.F5498.ALL,                      
//             DISP=SHR                                                 
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(TW1497A5),DISP=SHR   /* TWRP2006      
//          DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                    
//**********************************************************************
//* REPORT - 5498 TEST-INDICATOR REPORT USING PGM TWRP2007              
//********************************************************************* 
//REPT070  EXEC PGM=NATB030,REGION=8M,COND=(0,NE),                      
//         PARM='SYS=&NAT'                                              
//SYSPRINT  DD SYSOUT=&JCLO                                             
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01   DD SYSOUT=(&REPT,&REPT1.H),                                 
//             RECFM=LS,LRECL=133,BLKSIZE=11571                        
//CMWKF01   DD DSN=&HLQ0..&JOBNM..RECON.F5498.ALL,                      
//             DISP=SHR                                                 
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(TW1570AS),DISP=SHR   /* TWRP2007      
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//*-------------------------------------------------------------------- 
//* 5498 SPLIT - TO SEGREGATE PERIOD-1 AND PERIOD-2 RECORDS             
//*-------------------------------------------------------------------- 
//SPLT080  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//    PARM='SYS=&NAT'                                                   
//SYSPRINT DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSOUT   DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMWKF01  DD DSN=&HLQ0..&JOBNM..RECON.F5498.ALL,DISP=SHR               
//CMWKF02  DD  DSN=&HLQ0..&JOBNM..RECON.F5498.P2,                       
//             DISP=(NEW,CATLG,DELETE),                                 
//             DCB=(RECFM=LS,LRECL=1005),                               
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT,               
//             DATACLAS=&DATACLS                                        
//CMWKF03  DD  DSN=&HLQ0..&JOBNM..RECON.F5498.P1,                       
//             DISP=(NEW,CATLG,DELETE),                                 
//             DCB=(RECFM=LS,LRECL=1005),                               
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT,               
//             DATACLAS=&DATACLS                                        
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(TW1497A6),DISP=SHR  /* TWRP4712        
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                      
//*                                                                     
//*-------------------------------------------------------------------- 
//* REPORT - 5498 RECON REPORT FOR PERIOD-1 USING PROGRAM TWRP4711      
//*-------------------------------------------------------------------- 
//P1RP090  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//    PARM='SYS=&NAT'                                                   
//SYSPRINT DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSOUT   DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMPRT01  DD  SYSOUT=(&REPT,&REPT1.I),                                 
//             RECFM=LS,LRECL=133,BLKSIZE=11571                        
//CMPRT02  DD  SYSOUT=(&REPT,&REPT1.J),                                 
//             RECFM=LS,LRECL=133,BLKSIZE=11571                        
//CMPRT03  DD  SYSOUT=(&REPT,&REPT1.K),                                 
//             RECFM=LS,LRECL=133,BLKSIZE=11571                        
//CMWKF01  DD DSN=&HLQ0..&JOBNM..RECON.F5498.P1,DISP=SHR                
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(TW1497A2),DISP=SHR  /* TWRP4711        
//         DD DSN=&LIB1..PARMLIB(TW1497A7),DISP=SHR  /* PARM P1         
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                      
//*                                                                     
//*-------------------------------------------------------------------- 
//* REPORT - 5498 RECON REPORT FOR PERIOD-2 USING PROGRAM TWRP4711      
//*-------------------------------------------------------------------- 
//P2RP100  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//    PARM='SYS=&NAT'                                                   
//SYSPRINT DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSOUT   DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMPRT01  DD  SYSOUT=(&REPT,&REPT1.L),                                 
//             RECFM=LS,LRECL=133,BLKSIZE=11571                        
//CMPRT02  DD  SYSOUT=(&REPT,&REPT1.M),                                 
//             RECFM=LS,LRECL=133,BLKSIZE=11571                        
//CMPRT03  DD  SYSOUT=(&REPT,&REPT1.N),                                 
//             RECFM=LS,LRECL=133,BLKSIZE=11571                        
//CMWKF01  DD DSN=&HLQ0..&JOBNM..RECON.F5498.P2,DISP=SHR                
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(TW1497A2),DISP=SHR  /* TWRP4711        
//         DD DSN=&LIB1..PARMLIB(TW1497A8),DISP=SHR  /* PARM P2         
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                      
//*                                                                     
