//P9000IIW PROC  DBID='003',
//         REGN1='5M',
//         NAT='NAT003P',
//         JCLO='*',
//         NATMEM='PANNSEC',
//         SPC1='4',
//         SPC2='2',
//         SPC3='50',
//         SPC4='10',
//         SPC5='5',
//         DMPS='U',
//         LIB1='PROD',
//         LIB2='PROD',
//         HLQ8='PPDD'
//*
//*===================================================================*
//*
//*                        IIS/ERISA REPORTS
//*                          JANUARY  1999
//*
//*              IIS/ERISA CONFLICT REPORTS
//*
//*********************************************************************
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,PARM='SYS=&NAT'                
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS                                              
//CMPRINT  DD SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ8..P9000IIW.EXTR1,                                
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            RECFM=LS,LRECL=4000
//CMWKF02  DD DSN=&HLQ8..P9000IIW.EXTR2,                                
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC3,&SPC4),RLSE),
//            RECFM=LS,LRECL=4000
//CMWKF03  DD DSN=&HLQ8..P9000IIW.EXTR3,                                
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            RECFM=LS,LRECL=4000
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P9000II1),DISP=SHR                     
//**********************************************************************
//SORT020 EXEC PGM=SORT,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SORTIN   DD DSN=&HLQ8..P9000IIW.EXTR1,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..P9000IIW.SORT1,
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            SPACE=(CYL,(4,2),RLSE),
//            RECFM=LS,LRECL=4000
//SYSIN    DD DSN=&LIB1..PARMLIB(P9000II2),DISP=SHR
//**********************************************************************
//SORT030 EXEC PGM=SORT,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SORTIN   DD DSN=&HLQ8..P9000IIW.EXTR2,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..P9000IIW.SORT2,
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC3,&SPC4),RLSE),
//            RECFM=LS,LRECL=4000
//SYSIN    DD DSN=&LIB1..PARMLIB(P9000II3),DISP=SHR
//**********************************************************************
//SORT040 EXEC PGM=SORT,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SORTIN   DD DSN=&HLQ8..P9000IIW.EXTR3,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..P9000IIW.SORT3,
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            RECFM=LS,LRECL=4000
//SYSIN    DD DSN=&LIB1..PARMLIB(P9000II4),DISP=SHR
//*********************************************************************
//REPT050 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                   
//  PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS                                              
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(8,II9000W1)
//CMPRT02  DD SYSOUT=(8,II9000W2)
//CMPRT03  DD SYSOUT=(8,II9000W3)
//CMWKF01  DD DSN=&HLQ8..P9000IIW.SORT1,DISP=SHR                        
//CMWKF02  DD DSN=&HLQ8..P9000IIW.SORT2,DISP=SHR                        
//CMWKF03  DD DSN=&HLQ8..P9000IIW.SORT3,DISP=SHR                        
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P9000II5),DISP=SHR                     
//*********************************************************************
//EXTR060 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                   
//  PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS                                              
//CMPRINT  DD SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ8..P9000IIW.EXTR4,                                
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC4,&SPC5),RLSE),
//            RECFM=LS,LRECL=4000
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P9000II6),DISP=SHR                     
//**********************************************************************
//SORT070 EXEC PGM=SORT,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SORTIN   DD DSN=&HLQ8..P9000IIW.EXTR4,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..P9000IIW.SORT4,
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC4,&SPC5),RLSE),
//            RECFM=LS,LRECL=4000
//SYSIN    DD DSN=&LIB1..PARMLIB(P9000II7),DISP=SHR
//*********************************************************************
//REPT080 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                   
//  PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS                                              
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(8,II9000W4)
//CMWKF01  DD DSN=&HLQ8..P9000IIW.SORT4,DISP=SHR                        
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P9000II8),DISP=SHR                     
//*********************************************************************
//EXTR090 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                   
//  PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS                                              
//CMPRINT  DD SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ8..P9000IIW.EXTR5,                                
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC3,&SPC4),RLSE),
//            RECFM=LS,LRECL=200
//CMWKF02  DD DSN=&HLQ8..P9000IIW.EXTR6,                                
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC3,&SPC4),RLSE),
//            RECFM=LS,LRECL=200
//CMWKF03  DD DSN=&HLQ8..P9000IIW.EXTR7,                                
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC3,&SPC4),RLSE),
//            RECFM=LS,LRECL=200
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P9000II9),DISP=SHR                     
//**********************************************************************
//SORT100 EXEC PGM=SORT,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SORTIN   DD DSN=&HLQ8..P9000IIW.EXTR5,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..P9000IIW.SORT5,
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC3,&SPC4),RLSE),
//            RECFM=LS,LRECL=200
//SYSIN    DD DSN=&LIB1..PARMLIB(P9000IIA),DISP=SHR
//**********************************************************************
//SORT110 EXEC PGM=SORT,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SORTIN   DD DSN=&HLQ8..P9000IIW.EXTR6,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..P9000IIW.SORT6,
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC3,&SPC4),RLSE),
//            RECFM=LS,LRECL=200
//SYSIN    DD DSN=&LIB1..PARMLIB(P9000IIB),DISP=SHR
//**********************************************************************
//SORT120 EXEC PGM=SORT,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SORTIN   DD DSN=&HLQ8..P9000IIW.EXTR7,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..P9000IIW.SORT7,
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC3,&SPC4),RLSE),
//            RECFM=LS,LRECL=200
//SYSIN    DD DSN=&LIB1..PARMLIB(P9000IIC),DISP=SHR
//*********************************************************************
//REPT130 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                   
//  PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS                                              
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(8,II9000W5)
//CMPRT02  DD SYSOUT=(8,II9000W6)
//CMPRT03  DD SYSOUT=(8,II9000W7)
//CMWKF01  DD DSN=&HLQ8..P9000IIW.SORT5,DISP=SHR                        
//CMWKF02  DD DSN=&HLQ8..P9000IIW.SORT6,DISP=SHR                        
//CMWKF03  DD DSN=&HLQ8..P9000IIW.SORT7,DISP=SHR                        
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P9000IID),DISP=SHR                     
