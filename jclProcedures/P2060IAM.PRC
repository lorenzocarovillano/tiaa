//P2060IAM PROC DBID='003',
//         DMPS='U',
//         HLQ0='PIA.ONL.ANN',
//         HLQ1='PIA.ANN',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='8M',
//         REPT='8',
//         SPC1='1',
//         SPC2='1'
//*
//* ----------------------------------------------------------------- *
//* PROCEDURE:  P2060IAM                 DATE:     5/06/98            *
//* ----------------------------------------------------------------- *
//* PROGRAMS EXECUTED: IAAP775 (NATURAL) = P                          *
//*                                                                   *
//* ----------------------------------------------------------------- *
//* THIS JOB IS FOR GENERATING A PARM CARD FOR P1930IAM WHICH         *
//* DETERMINES THE FACTOR TYPE TO BE USED                             *
//* READS ADABAS FILE 202 DBID=003                                    *
//*                                                                   *
//* ----------------------------------------------------------------- *
//*                                                                     
//* MODIFICATION HISTORY
//* 08/12/98   ADDED  OPRB PARAMETER                                 *
//* 04/03/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//*******************************************************
//*                                                                     
//REPT005 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM=('SYS=&NAT',
//       'OPRB=(.DBID=253,FNR=043,I,A)')
//SYSPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01   DD SYSOUT=(&REPT,IA2060M1)
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSUDUMP  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DIAUVS    DD DSN=&HLQ0..DAILYAUV.V471,DISP=SHR
//CMWKF01   DD DSN=&HLQ1..IAIQ.FACTOR.CNTRL.PARM,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,SPACE=(TRK,(&SPC1,&SPC2),RLSE),
//             DCB=RECFM=LS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P2060IA2),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
