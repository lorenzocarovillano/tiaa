//P5240IAA PROC DBID='003',
//         DMPS='U',
//         HLQ0='PIA.ONL.ANN',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8'
//*
//* ----------------------------------------------------------------- * 
//* PROCEDURE:  P5240IAA                 DATE:    03/01/97            * 
//* ----------------------------------------------------------------- * 
//* PROGRAMS EXECUTED: IAAP391 (NATURAL)                                
//* ----------------------------------------------------------------- * 
//* UPDATE ANNUALLY REVALUED UNIT BASED
//* PAYMENTS FOR THE IA-ADMIN MAY CHECKS ON FILE 201
//* ----------------------------------------------------------------- * 
//*                                                                     
//* MODIFICATION HISTORY
//* 04/12/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//**********************************************************************
//*
//UPDT010 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01   DD SYSOUT=(&REPT,IA5240A1)
//SYSUDUMP  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DIAUVS    DD DSN=&HLQ0..DAILYAUV.V471,DISP=SHR
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P5240IA1),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
