//P1935TWA PROC DBID=003,
//             STATE=35,
//             DMPS='U',
//*            GDG0='+0',
//             GDG1='+1',
//             ORIGGDG='+0',
//             HLQ1='PPDT.TAX',
//             LIB1='PROD',             PARMLIB
//             LIB2='PROD',             NT2LOGON.PARMLIB
//             NAT='NAT003P',
//             NATMEM='PANNSEC',
//             JCLO='*',
//             REGN1='9M',
//             REPT='8',
//             SPC1='10,5',
//             UNIT1='SYSDA',
//             UNIT2='CARTV',
//             UNIT3='CARTN'
//*********************************************************************
//* PROCEDURE: P1935TWA NY STATE REPORTING CORRECTIONS                *
//*            NEED TO PASS CORRECT TAX YEAR THROUGH P1935TW1 PARMLIB *
//*            AT FRMT010 STEP TO GET THE RIGHT CORRECTION FILE.      *
//*            ALSO NEED TO SELECT PROPER GDG VERSION OF THE ORIGINAL *
//*            FILE AT EXTR020 STEP.                                  *
//*********************************************************************
//* FRMT010  EXTRACT FROM ADABAS FILE FOR STATE SPECIFIED IN JOB CARD.*
//*-------------------------------------------------------------------*
//FRMT010 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT'
//STEPLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//          DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//SYSPRINT  DD SYSOUT=&JCLO
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//SORTWK01  DD UNIT=&UNIT1,SPACE=(CYL,(&SPC1))
//SORTWK02  DD UNIT=&UNIT1,SPACE=(CYL,(&SPC1))
//SORTWK03  DD UNIT=&UNIT1,SPACE=(CYL,(&SPC1))
//SORTWK04  DD UNIT=&UNIT1,SPACE=(CYL,(&SPC1))
//SORTWK05  DD UNIT=&UNIT1,SPACE=(CYL,(&SPC1))
//SORTWK06  DD UNIT=&UNIT1,SPACE=(CYL,(&SPC1))
//SORTWK07  DD UNIT=&UNIT1,SPACE=(CYL,(&SPC1))
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=&REPT
//CMPRT02   DD SYSOUT=&REPT
//CMPRT03   DD SYSOUT=&REPT
//CMPRT04   DD SYSOUT=&REPT
//CMPRT05   DD SYSOUT=&REPT
//CMPRT06   DD SYSOUT=&REPT
//CMPRT07   DD SYSOUT=&REPT
//CMPRT08   DD SYSOUT=&REPT
//CMPRT09   DD SYSOUT=&REPT
//CMPRT10   DD SYSOUT=(&REPT,TW19&STATE.AA)
//CMPRT11   DD SYSOUT=(&REPT,TW19&STATE.AB)
//CMWKF01   DD DSN=&HLQ1..FRMT010.NY.CORR.TAPE.TEMP,
//             DISP=(,CATLG,DELETE),UNIT=&UNIT2,
//             VOL=(,RETAIN),TRTCH=NOCOMP,
//             DCB=(RECFM=LS,LRECL=128,BLKSIZE=6400)
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//**        DD DSN=&LIB1..PARMLIB(P1935TW1),DISP=SHR
//          DD DSN=&LIB1..CNTL.PARMLIB(P1935TW1),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* TWRP3527
//*-------------------------------------------------------------------*
//* EXTR020  COMPARE ORIGINAL & CORRECTION FILE TO CREATE TIAA & TRUST*
//*-------------------------------------------------------------------*
//EXTR020 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT'
//STEPLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//          DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//SYSPRINT  DD SYSOUT=&JCLO
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMWKF01   DD DSN=&HLQ1..FRMT010.NY.CORR.TAPE.TEMP,DISP=SHR
//CMWKF02   DD DSN=&HLQ1..COPY070.NY.VR(&ORIGGDG),DISP=SHR
//CMWKF03   DD DSN=&&TIAA,
//             DISP=(NEW,PASS,DELETE),UNIT=&UNIT1,
//             VOL=(,RETAIN),TRTCH=NOCOMP,
//             DCB=(RECFM=LS,LRECL=128,BLKSIZE=6400)
//CMWKF04   DD DSN=&&TRUST,
//             DISP=(NEW,PASS,DELETE),UNIT=&UNIT1,
//             VOL=(,RETAIN),TRTCH=NOCOMP,
//             DCB=(RECFM=LS,LRECL=128,BLKSIZE=6400)
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P1935TW3),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* TWRP3556
//*-------------------------------------------------------------------*
//* SORT030 - SORT TIAA FILE                                          *
//*-------------------------------------------------------------------*
//SORT030  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//SORTIN   DD DSN=&&TIAA,DISP=(OLD,PASS)
//SORTOUT  DD DSN=&&TIAASRT,
//            DISP=(NEW,PASS,DELETE),UNIT=&UNIT1,
//            VOL=(,RETAIN),TRTCH=NOCOMP,
//            DCB=(RECFM=LS,LRECL=128,BLKSIZE=6400)
//SYSIN    DD DSN=&LIB1..PARMLIB(P1935TW4),DISP=SHR
//*-------------------------------------------------------------------*
//* SORT040 - SORT TRUST FILE                                         *
//*-------------------------------------------------------------------*
//SORT040  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//SORTIN   DD DSN=&&TRUST,DISP=(OLD,PASS)
//SORTOUT  DD DSN=&&TRUSTSRT,
//            DISP=(NEW,PASS,DELETE),UNIT=&UNIT1,
//            VOL=(,RETAIN),TRTCH=NOCOMP,
//            DCB=(RECFM=LS,LRECL=128,BLKSIZE=6400)
//SYSIN    DD DSN=&LIB1..PARMLIB(P1935TW4),DISP=SHR
//*-------------------------------------------------------------------*
//* MERG050 - MERGE TIAA AND TRUST FILES                              *
//*-------------------------------------------------------------------*
//MERG050  EXEC PGM=IEBGENER,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//SYSUT1   DD DSN=&&TIAASRT,DISP=(OLD,PASS)
//         DD DSN=&&TRUSTSRT,DISP=(OLD,PASS)
//SYSUT2   DD DSN=&HLQ1..FRMT010.NY.CORR.TAPE,
//            DISP=(,CATLG,DELETE),UNIT=&UNIT2,
//            VOL=(,RETAIN),TRTCH=NOCOMP,
//            DCB=(RECFM=LS,LRECL=128,BLKSIZE=6400)
//SYSIN    DD DUMMY
//*-------------------------------------------------------------------*
//* COPY060 - STEP CREATES A COPY OF THE STATE CORRECTION REPORTING   *
//*           FILE FOR VR (VITAL RECORDS) TO BE STORED IN IRON        *
//*           MOUNTAIN.                                               *
//*-------------------------------------------------------------------*
//COPY060 EXEC PGM=IEBGENER,COND=(0,NE)
//SYSPRINT  DD SYSOUT=&JCLO
//SYSUT1    DD DSN=&HLQ1..FRMT010.NY.CORR.TAPE,
//             DISP=OLD,UNIT=&UNIT2
//SYSUT2    DD DSN=&HLQ1..FRMT010.NY.CORR.TAPE.VR(&GDG1),
//             DISP=(,CATLG,DELETE),
//             UNIT=&UNIT3,
//             DCB=MODLDSCB,RECFM=LS,LRECL=128
//SYSIN     DD DUMMY
//*-------------------------------------------------------------------*
//* CREA070 - PROGRAM WILL PRODUCE A TAPE DUMP REPORT OF THE STATE OF *
//*           NEW YORK CORRECTION REPORTING TAPE FILE.                *
//*-------------------------------------------------------------------*
//CREA070 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT'
//STEPLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//          DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//SYSPRINT  DD SYSOUT=&JCLO
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=(&REPT,TW1935AC)
//SYSUDUMP  DD SYSOUT=&DMPS
//CMWKF01   DD DSN=&HLQ1..FRMT010.NY.CORR.TAPE,
//             DISP=OLD,UNIT=&UNIT2
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P180BTWH),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* TWRP3640
