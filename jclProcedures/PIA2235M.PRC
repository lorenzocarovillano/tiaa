//PIA2235M PROC GDG0='+0',
//         DBID='003',
//         DMPS='U',
//         HLQ0='PPDD',
//         HLQ1='PPDT',
//         HLQ2='PIA.ANN',
//         HLQ3='PIA.ONL.ANN',
//         JCLO='*',
//         JOBNM1='PIA2235M',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         SPC1='500',
//         SPC2='300',
//         SPC3='100',
//         NAT='NAT003P',
//         NATMEM='PANNSEC'
//*
//*******************************************************************
//*                                                                     
//* CREATES A FEED TO ODS OF ALL IA CONTRACTS. THIS WILL BE DONE AS     
//* PART OF IA MONTHLY PAYMENT SCHEDULE.                                
//* INPUT TO THIS JOB IS FROM P2210IAM.                              S  
//*                                                                     
//* MODIFICATION HISTORY
//* 07/22/2016 ORIGINAL VERSION
//*
//**********************************************************************
//DELT001  EXEC PGM=IEFBR14
//DD1      DD DSN=&HLQ2..&JOBNM1..IATRANS.RECTYP10,
//            DISP=(MOD,DELETE),
//            UNIT=SYSDA,
//            SPACE=(TRK,0)
//DD2      DD DSN=&HLQ2..&JOBNM1..IATRANS.RECTYP20,
//            DISP=(MOD,DELETE),
//            UNIT=SYSDA,
//            SPACE=(TRK,0)
//DD3      DD DSN=&HLQ2..&JOBNM1..IATRANS.RECTYP30,
//            DISP=(MOD,DELETE),
//            UNIT=SYSDA,
//            SPACE=(TRK,0)
//DD4      DD DSN=&HLQ2..&JOBNM1..IATRANS.RECTYP40,
//            DISP=(MOD,DELETE),
//            UNIT=SYSDA,
//            SPACE=(TRK,0)
//DD5      DD DSN=&HLQ2..&JOBNM1..IATRANS.HDRTRLR,
//            DISP=(MOD,DELETE),
//            UNIT=SYSDA,
//            SPACE=(TRK,0)
//*
//* ----------------------------------------------------------------- *
//* FORMATS FILE FROM P2210IAM
//* ------------------------------------------------------------------ *
//FRMT010 EXEC PGM=NATB030,COND=(0,NE),
//        PARM='SYS=&NAT,OPRB=(.ALL)'
//SYSPRINT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMWKF01  DD  DSN=&HLQ1..PNA.IAIQ.IATRANS.FINAL(&GDG0),
//             DISP=SHR
//CMWKF02  DD  DSN=&HLQ0..ANN.IAIQ.IATRANS.TEMP,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC1,&SPC3),RLSE),
//             UNIT=SYSDA,DCB=MODLDSCB,
//             RECFM=LS
//CMWKF03  DD  DSN=&LIB1..PARMLIB(IAPARMP),DISP=SHR
//DIAUVS   DD  DSN=&HLQ3..DAILYAUV.V471,DISP=SHR
//*
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(IA2235M1),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* ----------------------------------------------------------------- *
//* SPLIT FILE FROM PREVIOUS STEP INTO 5 FILES FOR TRANSFER
//* ----------------------------------------------------------------- *
//SPLT020 EXEC PGM=NATB030,COND=(0,NE),
//        PARM='SYS=&NAT,OPRB=(.ALL)'
//SYSPRINT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMWKF01  DD  DSN=&HLQ2..&JOBNM1..IATRANS.RECTYP10,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC2,&SPC3),RLSE),
//             UNIT=SYSDA,DCB=MODLDSCB,
//             RECFM=LS
//CMWKF02  DD  DSN=&HLQ2..&JOBNM1..IATRANS.RECTYP20,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC2,&SPC3),RLSE),
//             UNIT=SYSDA,DCB=MODLDSCB,
//             RECFM=LS
//CMWKF03  DD  DSN=&HLQ2..&JOBNM1..IATRANS.RECTYP30,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC2,&SPC3),RLSE),
//             UNIT=SYSDA,DCB=MODLDSCB,
//             RECFM=LS
//CMWKF04  DD  DSN=&HLQ2..&JOBNM1..IATRANS.RECTYP40,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(5,1),RLSE),
//             UNIT=SYSDA,DCB=MODLDSCB,
//             RECFM=LS
//CMWKF05  DD  DSN=&HLQ2..&JOBNM1..IATRANS.HDRTRLR,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(1,0),RLSE),
//             UNIT=SYSDA,DCB=MODLDSCB,
//             RECFM=LS
//CMWKF06  DD  DSN=&HLQ0..ANN.IAIQ.IATRANS.TEMP,DISP=SHR
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(IA2215M1),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* ----------------------------------------------------------------- *
