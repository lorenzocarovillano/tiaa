//PCP1100D PROC DBID=003,                                               
//         DMPS='U',                                                    
//         HLQ0='PCPS.ANN',         DASD HLQ                            
//         JOBNM='PCP1100D',        2ND HLQ FOR FILES CREATED IN PROC   
//         UNIT1=SYSDA,             UNIT PARM FOR DASD OUTPUT           
//         JCLO='*',                                                    
//         REPT='8',                                                    
//         GDG0='0',                                                    
//         GDG1='+1',                                                   
//         LIB1='PROD',                                                 
//         LIB2='PROD',                                                 
//         NAT='NAT003P',                                               
//         NATMEM='PANNSEC',                                            
//         NATPGM1='CP1100D3',                                          
//         PARMMEM1='CP1100D1',                                         
//         PARMMEM2='CP1100D2',                                         
//         REGN1='9M',                                                  
//         SPC1=1,                                                      
//         SPC2=10                                                      
//* ------------------------------------------------------------------- 
//SORT010 EXEC PGM=SORT,COND=(0,NE),REGION=5M                           
//SYSOUT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
//SYSPRINT DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR                              
//SORTIN   DD DSN=&HLQ0..&JOBNM..CHILDMDM(&GDG0),DISP=SHR               
//SORTOUT  DD DSN=&HLQ0..&JOBNM..CHILDMDM.SORTED,DISP=OLD               
//SYSIN    DD DSN=&LIB1..PARMLIB(&PARMMEM1),DISP=SHR                    
//SORT020 EXEC PGM=SORT,COND=(0,NE),REGION=5M                           
//SYSOUT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
//SYSPRINT DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR                              
//SORTIN   DD DSN=&HLQ0..&JOBNM..CHILDQDR(&GDG0),DISP=SHR               
//SORTOUT  DD DSN=&HLQ0..&JOBNM..CHILDQDR.SORTED,DISP=OLD               
//SYSIN    DD DSN=&LIB1..PARMLIB(&PARMMEM2),DISP=SHR                    
//REPT010  EXEC PGM=NATB030,REGION=&REGN1,PARM='SYS=&NAT'               
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR                              
//DDPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR                              
//CMPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR                              
//CMPRT01  DD SYSOUT=(&REPT,CP1100D1),                                  
//         DCB=(RECFM=VB,LRECL=136),OUTPUT=*.OUTVDR                     
//CMPRT02  DD SYSOUT=(&REPT,CP1100D2),                                  
//         DCB=(RECFM=VB,LRECL=136),OUTPUT=*.OUTVDR                     
//CMPRT03  DD SYSOUT=(&REPT,CP1100D3),                                  
//         DCB=(RECFM=VB,LRECL=136),OUTPUT=*.OUTVDR                     
//SYSUDUMP DD SYSOUT=&DMPS                                              
//CMWKF01 DD DSN=&HLQ0..&JOBNM..CHILDMDM.SORTED,DISP=OLD                
//CMWKF02 DD DSN=&HLQ0..&JOBNM..CHILDCPS(&GDG0),DISP=OLD                
//CMWKF03 DD DSN=&HLQ0..&JOBNM..CHILDCPS(&GDG1),                        
//            DISP=(,CATLG,DELETE),                                     
//            DCB=(RECFM=LS,LRECL=802,BLKSIZE=27268),UNIT=&UNIT1,       
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),                           
//            DATACLAS=DCPSEXTC                                         
//CMWKF04 DD DSN=&HLQ0..&JOBNM..CHILDTX(&GDG1),                         
//            DISP=(,CATLG,DELETE),                                     
//            DCB=(RECFM=LS,LRECL=800,BLKSIZE=28000),UNIT=&UNIT1,       
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),                           
//            DATACLAS=DCPSEXTC                                         
//CMWKF05 DD DSN=&HLQ0..&JOBNM..EXPAG(&GDG1),                           
//            DISP=(,CATLG,DELETE),                                     
//            DCB=(RECFM=LS,LRECL=236,BLKSIZE=28084),UNIT=&UNIT1,       
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),                           
//            DATACLAS=DCPSEXTC                                         
//CMWKF06 DD DSN=&HLQ0..&JOBNM..CTH(&GDG1),                             
//            DISP=(,CATLG,DELETE),                                     
//            DCB=(RECFM=LS,LRECL=236,BLKSIZE=28084),UNIT=&UNIT1,       
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),                           
//            DATACLAS=DCPSEXTC                                         
//CMWKF07 DD DSN=&HLQ0..&JOBNM..CHILDQDR.SORTED,DISP=OLD                
//CMWKF08 DD DSN=&HLQ0..&JOBNM..CHILDCPS.QDRO(&GDG0),DISP=OLD           
//CMWKF09 DD DSN=&HLQ0..&JOBNM..CHILDCPS.QDRO(&GDG1),                   
//            DISP=(,CATLG,DELETE),                                     
//            DCB=(RECFM=LS,LRECL=802,BLKSIZE=27268),UNIT=&UNIT1,       
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),                           
//            DATACLAS=DCPSEXTC                                         
//CMWKF10 DD DSN=&HLQ0..&JOBNM..SIEBEL(&GDG1),                          
//            DISP=(,CATLG,DELETE),                                     
//            DCB=(RECFM=LS,LRECL=172,BLKSIZE=27864),UNIT=&UNIT1,       
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),                           
//            DATACLAS=DCPSEXTC                                         
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(&NATPGM1),DISP=SHR                     
