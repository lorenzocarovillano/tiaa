//PCP1411D PROC HLQ1='PMS.ANN'                                          
//*****************************************************************     
//*   COPY AP CANCELS AND STOPS WITH DEDUCTION CODES OF 900+      *     
//*                                                               *     
//*****************************************************************     
//COPY020  EXEC PGM=IEBGENER,COND=(0,NE)                                
//SYSIN    DD DUMMY                                                     
//SYSPRINT DD SYSOUT=*                                                  
//SYSUT1   DD DSN=&HLQ1..P1400CPD.CANSTP.TXSCHD(-3),DISP=SHR            
//         DD DSN=&HLQ1..P1400CPD.CANSTP.TXSCHD(-2),DISP=SHR            
//         DD DSN=&HLQ1..P1400CPD.CANSTP.TXSCHD(-1),DISP=SHR            
//         DD DSN=&HLQ1..P1400CPD.CANSTP.TXSCHD(0),DISP=SHR             
//SYSUT2   DD DSN=&HLQ1..P1400CPD.CANSTP.TXSCHD(+1),                    
//         DISP=(NEW,CATLG,DELETE),                                     
//         SPACE=(CYL,(3,5)),UNIT=SYSDA,                                
//         DCB=(RECFM=LS,LRECL=205),                                    
//         DATACLAS=DCPSEXTC                                            
