//P1435TWR PROC                                                         
//*------------*                                                        
//* EXTRACTS MULTIPLE COUNTRIES WITH SAME PARTICIPANT CODES             
//* FROM PARTICIPANT DATABASE DB003/095                                 
//* PRINTS MULTIPLE COUNTRY REPORT.                                     
//*------------*                                                        
//EXTR010  EXEC PGM=NATB030,REGION=8M,                                  
//         PARM='SYS=NAT003P'                                           
//STEPLIB  DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR                          
//         DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR               
//DDCARD   DD  DSN=PROD.PARMLIB(DBAPP003),DISP=SHR                      
//CMWKF01  DD  DSN=PPDD.TAX.P1435TWR.PART.TA.EXT,UNIT=SYSDA,            
//             DISP=(NEW,CATLG,DELETE),                                 
//             SPACE=(CYL,(4,2),RLSE),                                  
//             DCB=RECFM=LS,LRECL=54                                    
//SYSPRINT DD  SYSOUT=*                                                 
//DDPRINT  DD  SYSOUT=*                                                 
//SYSOUT   DD  SYSOUT=*                                                 
//SYSUDUMP DD  SYSOUT=U                                                 
//CMPRINT  DD  SYSOUT=*                                                 
//CMPRT01  DD  SYSOUT=8                                                 
//CMPRT02  DD  SYSOUT=8                                                 
//CMSYNIN  DD  DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR              
//         DD  DSN=PROD.PARMLIB(P1435TW1),DISP=SHR                      
//         DD  DSN=PROD.PARMLIB(FINPARM),DISP=SHR                       
//*------------*                                                        
//* SORT EXTRACT BY COUNTRY CODE AND PIN.                               
//*------------*                                                        
//SORT020 EXEC PGM=SORT,REGION=8M,COND=(0,NE)                           
//SYSOUT   DD  SYSOUT=*                                                 
//SYSUDUMP DD  SYSOUT=U                                                 
//SYSPRINT DD  SYSOUT=*                                                 
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(10,5),RLSE)                       
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(10,5),RLSE)                       
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(10,5),RLSE)                       
//SORTIN   DD  DSN=PPDD.TAX.P1435TWR.PART.TA.EXT,DISP=SHR               
//SORTOUT  DD  DSN=PPDD.TAX.P1435TWR.PART.TA.EXT.SORTED,                
//             DISP=(NEW,CATLG,DELETE),                                 
//             UNIT=SYSDA,                                              
//             SPACE=(CYL,(4,2),RLSE),                                  
//             DCB=RECFM=LS,LRECL=54                                    
//SYSIN    DD  DSN=PROD.PARMLIB(P1435TW2),DISP=SHR                      
//*------------*                                                        
//* CREATES A REPORT OF MULTIPLE COUNTRY PARTICIPANTS                   
//*------------*                                                        
//REPT030  EXEC PGM=NATB030,REGION=8M,COND=(0,NE),                      
//         PARM='SYS=NAT003P'                                           
//STEPLIB  DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR                          
//         DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR               
//DDCARD   DD  DSN=PROD.PARMLIB(DBAPP003),DISP=SHR                      
//CMWKF01  DD  DSN=PPDD.TAX.P1435TWR.PART.TA.EXT.SORTED,DISP=SHR        
//SYSPRINT DD  SYSOUT=*                                                 
//DDPRINT  DD  SYSOUT=*                                                 
//SYSOUT   DD  SYSOUT=*                                                 
//SYSUDUMP DD  SYSOUT=U                                                 
//CMPRINT  DD  SYSOUT=*                                                 
//CMPRT01  DD  SYSOUT=8                                                 
//CMPRT02  DD  SYSOUT=8                                                 
//CMSYNIN  DD  DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR              
//         DD  DSN=PROD.PARMLIB(P1435TW3),DISP=SHR                      
//         DD  DSN=PROD.PARMLIB(FINPARM),DISP=SHR                       
