/bin/awk '
BEGIN { FS = "=" }
{
  line[NR]= substr($0,0,72)
  pcb=0
  segdummy=0
}
END {
 for (nline = 1 ; nline <= NR ; nline++){
   if (line[nline] ~ /.*INCLUDE MEMBER\=/ && line[nline] !~ /\/\/\*/ ){
     if (line[nline] ~ /\&/ || line[nline] ~ /\(/){
       print "//*MSYS " line[nline] 
     } else {
       include=""
       gsub(/\ \ /," ",line[nline])
       begin=index(line[nline],"MEMBER")+7
       include=substr(line[nline],begin,length(line[nline])-begin+1)
       if ( index(include," ") > 0 )
       {
        include=substr(include,1,index(include," "))
       }
       gsub(/ /,"",include)
       print "//*MSYS " line[nline] 
       command="cat ./INCLUDE/" include
       system(command)
       close(command)
     }
   } else {
     print line[nline]
   }

 }
}
'
