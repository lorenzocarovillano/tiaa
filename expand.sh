#!/bin/ksh

#dos2unix JCL/*/*
#dos2unix PROC/*/*
rm -f ./newjcl/*
rm -f ./newproc/*

for job in JCL/*/*.JCL
do
  echo "in:$job"
  export jobname=`echo $job | cut -d'/' -f3 `
#  echo "$jobname"
  cat $job | . ./expand_include.awk > newjcl/$jobname
done
for proc in PROC/*/*.PRC
do
  echo "inProc:$proc"
  export jobname=`echo $proc | cut -d'/' -f3 `
#  echo "$jobname"
  cat $proc | . ./expand_include.awk > newproc/$jobname
done


