#!/bin/ksh

rm -f newjcl/*
rm -f newproc/*

for jcl in ./jcl/*
do
export jclName=`echo $jcl | cut -d'/' -f3 `
echo $jclName
cat $jcl | . ./fix_dcb.awk > newjcl/$jclName
done
for jcl in ./jclProcedures/*
do
export jclName=`echo $jcl | cut -d'/' -f3 `
echo $jclName
cat $jcl | . ./fix_dcb.awk > newproc/$jclName
done

unix2dos newjcl/*
unix2dos newproc/*
