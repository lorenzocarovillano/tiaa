/bin/awk '
BEGIN { FS = "=" }
{
  line[NR]= $0
}
END {
# value="NATB030"
# <batchlet ref="StandardBatchlet"/>
#<batchlet ref="NaturalProgramBatchlet"/>
 found=0
 for (nline = 1 ; nline <= NR ; nline++){
   if ( ( line[nline] ~ /property name=\"EXEC\"/ ) && ((line[nline] ~ /value=\"NATB030\"/  ) || (line[nline] ~ /value=\"\$\{NATVERS/ ) || (line[nline] ~ /value=\"\$\{NAT\}/ )    ) ){
     found=1
   }
   if (found > 0 && (line[nline] ~ /batchlet ref=\"/  ) ){
     gsub(/StandardBatchlet/,"NaturalProgramBatchlet",line[nline])
     found=0
   }
   print line[nline]
 }
}
'
